-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-10-2012 a las 20:57:23
-- Versión del servidor: 5.5.20
-- Versión de PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Categorias de los productos' AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombres`, `descripcion`, `estado`) VALUES
(1, 'Antigripales', 'Son para Contrarestar la gripe', '1'),
(2, 'Ampollas', 'Son para Contrarestar la gripe', '1'),
(3, 'Pastillas', 'Son para Contrarestar la gripe', '1'),
(4, 'Cremas', 'Son para Contrarestar la gripe', '1'),
(5, 'Jarabes', 'Son para Contrarestar la gripe', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(80) DEFAULT NULL,
  `razonsocial` varchar(80) DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `ruc` varchar(45) DEFAULT NULL,
  `direccion` varchar(80) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `estado` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombres`, `apellidos`, `razonsocial`, `dni`, `ruc`, `direccion`, `correo`, `telefono`, `estado`) VALUES
(0, 'Cliente', 'Cliente', 'Cliente', NULL, NULL, NULL, NULL, NULL, '1'),
(1, 'Juan', 'Rojas', NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(2, 'Maria', 'Torre', NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(3, 'Jonas', 'Puerta', NULL, NULL, NULL, NULL, NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleoperacion`
--

CREATE TABLE IF NOT EXISTS `detalleoperacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productos_id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT '0',
  `subtotal` decimal(5,2) DEFAULT '0.00',
  `igv` decimal(5,2) DEFAULT '0.00',
  `descuento` decimal(5,2) DEFAULT '0',
  `estado` char(1) DEFAULT '0',
  `operaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detalleOperacion_productos` (`productos_id`),
  KEY `fk_detalleOperacion_operaciones1` (`operaciones_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registrara los productos de la operacion' AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `detalleoperacion`
--

INSERT INTO `detalleoperacion` (`id`, `productos_id`, `cantidad`, `subtotal`, `igv`, `descuento`, `estado`, `operaciones_id`) VALUES
(14, 1, 2, '0.00', '0.00', NULL, NULL, 1),
(19, 1, 2, '0.00', '0.00', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE IF NOT EXISTS `documentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Documentos con los que se registran las operaciones' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `nombres`, `descripcion`, `estado`) VALUES
(1, 'Boleta', 'Sin IGV', '1'),
(2, 'Factura', 'Con IGV', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operaciones`
--

CREATE TABLE IF NOT EXISTS `operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_at` datetime DEFAULT NULL,
  `idusuario` int(11) DEFAULT '0',
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `clientes_id` int(11) NOT NULL,
  `parametros_id` int(11) NOT NULL,
  `proveedores_id` int(11) NOT NULL,
  `documentos_id` int(11) NOT NULL,
  `tipooperacion_id` int(11) NOT NULL,
  `importe` decimal(5,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_operaciones_clientes` (`clientes_id`),
  KEY `fk_operaciones_parametros1` (`parametros_id`),
  KEY `fk_operaciones_proveedores1` (`proveedores_id`),
  KEY `fk_operaciones_documentos1` (`documentos_id`),
  KEY `fk_operaciones_tipooperacion1` (`tipooperacion_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registrara tipo de documento y tipo de operacion' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `operaciones`
--

INSERT INTO `operaciones` (`id`, `fecha_at`, `idusuario`, `descripcion`, `estado`, `clientes_id`, `parametros_id`, `proveedores_id`, `documentos_id`, `tipooperacion_id`) VALUES
(1, '2012-10-26 13:29:30', 1, NULL, '1', 0, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

CREATE TABLE IF NOT EXISTS `parametros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `valor` decimal(5,2) DEFAULT '0.00',
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Descuentos y/o impuestos' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `parametros`
--

INSERT INTO `parametros` (`id`, `nombres`, `valor`, `descripcion`, `estado`) VALUES
(1, 'IGV', '0.18', 'Impuesto general a las ventas', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `precio` decimal(5,2) DEFAULT '0.00',
  `stock` int(11) DEFAULT '0',
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `categorias_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productos_categorias1` (`categorias_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombres`, `precio`, `stock`, `descripcion`, `estado`, `categorias_id`) VALUES
(1, 'Pastillas', '1.35', 20, NULL, '1', 1),
(2, 'Ampollas', '2.50', 9, NULL, '0', 2),
(3, 'Jarabes', '4.99', 7, NULL, '1', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(80) DEFAULT NULL,
  `razonsocial` varchar(80) DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `ruc` varchar(45) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `estado` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipooperacion`
--

CREATE TABLE IF NOT EXISTS `tipooperacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `estado` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tipos de Operacion' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tipooperacion`
--

INSERT INTO `tipooperacion` (`id`, `nombres`, `descripcion`, `estado`) VALUES
(1, 'Venta', 'Venta de productos', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `nombres` varchar(50) DEFAULT NULL COMMENT '							',
  `apellidos` varchar(80) DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `estado` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Usuarios del sistema' AUTO_INCREMENT=1 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `login`, `password`, `nombres`, `apellidos`, `dni`, `telefono`, `direccion`, `correo`, `estado`) VALUES
(1, 'gcastro', 'rpp_2012', 'Gerson', 'CASTRO HERRERA', '47586932', '988977630', 'Pedregal Alto', 'gersoncastro88@gmail.com', '1'),
(2, 'gguadalupe', 'rpp_2012', 'Giovana', 'GUADALUPE', '47586932', '988977630', 'Pedregal Alto', 'gi.ja.gu@gmail.com', '1'),
(3, 'npalomino', 'rpp_2012', 'Nick', 'PALOMINO PEREZ', '47586932', '988977630', '3 de Octubre', 'nbpalomino@gmail.com', '1'),
(4, 'dvela', 'rpp_2012', 'Daniela', 'VELA PONCE', '12345678', '324567818', 'Pedregal', 'danibb@hotmail.com', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
