<?php 
Load::models('profesores','detalleauxiliares','grupos','detallegrupos','alumnos');
class Detalles
{
	public static function grupos($curso,$profe)
	{
		$Grupos = new Grupos();
		$code = '';
		$code .= '<table>';
			$code .= '<tr>';
				$code .= '<th colspan="2">Grupos del curso</th>';
			$code .= '</tr>';
			$code .= '<ul>';
			$n=0;
			foreach($Grupos->find('conditions: cursos_id='.$curso) as $grupo):
				$n++;
				$code .= '<tr>';
					$code .= '<li type="none">';
						$code .= '<td>';
								$code .= '&nbsp;'.$n.'. '.$grupo->nombres;
						$code .= '</td>';
						$code .= '<td>';
								$code .= '&nbsp;'.Html::link('grupos/detalle/'.$grupo->id.'/'.$profe,'Integranes');
						$code .= '</td>';
					$code .= '</li>';
				$code .= '</tr>';
			endforeach;
			$code .= '</ul>';
		$code .= '</table>';
		echo $code;
	}
	public static function detalleGrupos($grupo)
	{
		$Detallegrupos = new Detallegrupos();
		$Alumnos = new Alumnos();
		$code = '';
		$code .= '<table>';
			$code .= '<tr>';
				$code .= '<th>Integrantes del curso</th>';
			$code .= '</tr>';
			$code .= '<ul>';
			$n=0;
			foreach($Detallegrupos->find('conditions: grupos_id='.$grupo) as $detalle):
				$integrante = $Alumnos->find_first('conditions: id='.$detalle->alumnos_id);
				$n++;
				$code .= '<tr>';
					$code .= '<li type="none">';
						$code .= '<td>';
								$code .= '&nbsp;'.$n.'. '.$integrante->nombres.' '.$integrante->apellidos;
						$code .= '</td>';
					$code .= '</li>';
				$code .= '</tr>';
			endforeach;
			$code .= '</ul>';
		$code .= '</table>';
		echo $code;
	}
	public static function auxiliaresCurso($uxiliar)
	{
		$Detalle = new Detalleauxiliares();
		$Profesor = new Profesores();
		$code = '';
		$code .= '<table>';
			$code .= '<tr>';
				$code .= '<th>Auxiliares del curso</th>';
			$code .= '</tr>';
			$code .= '<ul>';
			$n=0;	
			foreach($Detalle->find("conditions: auxiliares_id=".$uxiliar) as $auxiliares):
				$n++;
				$code .= '<tr>';
					$profe = $Profesor->find_first('conditions: id='.$auxiliares->profesores_id);
					$code .= '<td>';
						$code .= '<li type="none">';
							$code .= '&nbsp;'.$n.'. '.$profe->nombres.' '.$profe->apellidos;
						$code .= '</li>';
					$code .= '</td>';
				$code .= '</tr>';
			endforeach;
			$code .= '</ul>';
		$code .= '</table>';
		echo $code;
	}
}
?>
