<?php
Load::models('usuarios');
class UsuariosController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Usuarios = new Usuarios();
		$this->verUsuarios = $Usuarios->find();
	}
	public function create()
	{
		if(Input::hasPost('usuarios'))
		{
			$Usuarios = new Usuarios(Input::post('usuarios'));
			if($Usuarios->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Usuarios = new Usuarios();
		if(Input::hasPost('usuarios')){
			if($Usuarios->update(Input::post('usuarios'))){
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->usuarios = $Usuarios->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Usuarios = new Usuarios();
		if($Usuarios->delete((int)$id)){
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
}