<?php
Load::models('clientes');
class ClientesController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Clientes = new Clientes();
		$this->verClientes = $Clientes->find('conditions: id!=0');
	}
	public function create()
	{
		if(Input::hasPost('clientes'))
		{
			$Clientes = new Clientes(Input::post('clientes'));
			if($Clientes->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Clientes = new Clientes();
		if(Input::hasPost('clientes')){
			if($Clientes->update(Input::post('clientes'))){
				Input::delete();
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->clientes = $Clientes->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Clientes = new Clientes();
		if($Clientes->delete((int)$id)){
			Input::delete();
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
	public function listarClientes()
	{
		$input = $_GET["q"];
		$Clientes = new Clientes();
		//$this->clientes = $Clientes->find("conditions: nombres like '%$input%'");
		$this->clientes = $Clientes->find("nombres like '%$input%' or razonsocial like '%$input%' or dni like '%$input%' or ruc like '%$input%' or apellidos like '%$input%'");
	}
}





























