<?php
Load::models('categorias');
class CategoriasController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Categorias = new Categorias();
		$this->verCategorias = $Categorias->find();
	}
	public function create()
	{
		if(Input::hasPost('categorias'))
		{
			$Categorias = new Categorias(Input::post('categorias'));
			if($Categorias->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Categorias = new Categorias();
		if(Input::hasPost('categorias')){
			if($Categorias->update(Input::post('categorias'))){
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->categorias = $Categorias->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Categorias = new Categorias();
		if($Categorias->delete((int)$id)){
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
}