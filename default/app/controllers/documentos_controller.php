<?php
Load::models('documentos');
class DocumentosController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Documentos = new Documentos();
		$this->verDocumentos = $Documentos->find();
	}
	public function create()
	{
		if(Input::hasPost('documentos'))
		{
			$Documentos = new Documentos(Input::post('documentos'));
			if($Documentos->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Documentos = new Documentos();
		if(Input::hasPost('documentos')){
			if($Documentos->update(Input::post('documentos'))){
				Input::delete();
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->documentos = $Documentos->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Documentos = new Documentos();
		if($Documentos->delete((int)$id)){
			Input::delete();
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
}