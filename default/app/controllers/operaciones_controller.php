<?php
Load::models('operaciones','detalleoperacion','productos');
class OperacionesController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
	}
	public function ventas()
	{
		if(Input::hasPost('operaciones'))
		{
			$operaciones = new Operaciones(Input::post('operaciones'));
			$operaciones->idusuario = Auth::get('id');
			$operaciones->estado = '0';
			$operaciones->fecha = date('Y-m-d');
			//$operaciones->fecha = '2011-11-07';
			$id=Input::post('idcliente');
			if(Input::post('cliente')!='')
			{
				$idcliente = explode(' ',Input::post('cliente'));
				$id = $idcliente[0];
			}
			$operaciones->clientes_id = $id;
			if($operaciones->save()){
                Flash::valid('Operación exitosa');
                Input::delete();
                return Router::redirect('operaciones/detalle/'.$operaciones->id);               
            }else{
                Flash::error('Falló Operación');
            }
		}
	}
	public function listDetalles($idoperacion)
	{
		$this->idop=$idoperacion;
		$Detalleoperacion = new Detalleoperacion();	
		$this->listaDetalles = $Detalleoperacion->find('conditions: operaciones_id='.$idoperacion);
		//$this->importe=$Detalleoperacion->total($idoperacion);
	}
	public function borraritem($iddetalle,$idop)
	{
		$Detalle = new Detalleoperacion();
        if ($Detalle->delete((int)$iddetalle)) {
          	Flash::valid('Operación exitosa');
        }else{
         	Flash::error('Falló Operación'); 
        }
        return Router::redirect('operaciones/listDetalles/'.$idop);
	}
	public function detalle($idoperacion)
	{
		$this->idop=$idoperacion;
		$Operaciones = new Operaciones();
		$Detalleoperacion = new Detalleoperacion();	
		$this->operacion = $Operaciones->find_first('conditions: id='.$idoperacion);
		$this->listaDetalles = $Detalleoperacion->find('conditions: operaciones_id='.$idoperacion);
	}
	public function add()
	{
		if(Input::hasPost('producto'))
		{ 
			$idproducto = explode(' ',Input::post('producto'));
			$Productos = new Productos();
			$producto = $Productos->find_first('conditions: id ='.$idproducto[0]);
			$newdetalle = new Detalleoperacion(Input::post('detalleoperacion'));
			$newdetalle->productos_id = $idproducto[0];
			$newdetalle->cantidad = Input::post('cantidad');
			$newdetalle->subtotal = $producto->precio * Input::post('cantidad');
			$newdetalle->igv = Input::post('cantidad')*(0.18);
			$newdetalle->operaciones_id = Input::post('idoperacion');
			$newdetalle->fecha = date('Y-m-d');
			//$newdetalle->fecha = '2011-11-07';
			if($newdetalle->save()){
                Flash::valid('Operación exitosa');
                Input::delete();
				return Router::redirect("operaciones/listDetalles/".$newdetalle->operaciones_id);
            }else{
                Flash::error('Falló Operación');
				return Router::redirect("operaciones/listDetalles/");
				return Router::redirect("operaciones/listDetalles/". Input::post('idoperacion'));
            }
		}
	}
	public function confirmarventa($idoperacion)
	{
		$Operaciones = new Operaciones();
		$operacion = $Operaciones->find_first('conditions: id='.$idoperacion);
		$operacion->importe = Input::post('suma');
		if($operacion->save()){
			$Detalleoperacion = new Detalleoperacion();
			$this->detalles = $Detalleoperacion->find('conditions: operaciones_id='.$idoperacion);
			Flash::valid('Grasias por su conpra');
		}
	}
}
