<?php
Load::models('usuarios');
View::template('login');
class LoginController extends AppController
{
	public function index()
	{
        if (Input::hasPost("login","password")){
            $pwd = Input::post("password");
            $usuario=Input::post("login");
 
            $auth = new Auth("model", "class: usuarios", "login: $usuario", "password: $pwd");
            if ($auth->authenticate()) {
                Router::redirect("index/");
            } else {
                Flash::error("Falló");
            }
        }
    }
	public function salir()
	{
		Auth::destroy_identity();
		Flash::valid('Session cerrada');
		Router::redirect('index');
	}
}
