<?php
Load::models('proveedores');
class ProveedoresController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Proveedores = new Proveedores();
		$this->verProveedores = $Proveedores->find();
	}
	public function create()
	{
		if(Input::hasPost('proveedores'))
		{
			$Proveedores = new Proveedores(Input::post('proveedores'));
			if($Proveedores->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Proveedores = new Proveedores();
		if(Input::hasPost('proveedores')){
			if($Proveedores->update(Input::post('proveedores'))){
				Input::delete();
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->proveedores = $Proveedores->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Proveedores = new Proveedores();
		if($Proveedores->delete((int)$id)){
			Input::delete();
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
}