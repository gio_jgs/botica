<?php
Load::models('parametros');
class ParametrosController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Parametros = new Parametros();
		$this->verParametros = $Parametros->find();
	}
	public function create()
	{
		if(Input::hasPost('parametros'))
		{
			$Parametros = new Parametros(Input::post('parametros'));
			if($Parametros->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Parametros = new Parametros();
		if(Input::hasPost('parametros')){
			if($Parametros->update(Input::post('parametros'))){
				Input::delete();
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->parametros = $Parametros->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Parametros = new Parametros();
		if($Parametros->delete((int)$id)){
			Input::delete();
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
}