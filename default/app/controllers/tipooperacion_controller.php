<?php
Load::models('tipooperacion');
class TipooperacionController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
	}
	public function create()
	{
		if(Input::hasPost('tipooperacion'))
		{
			$Tipooperacion = new Tipooperacion(Input::post('tipooperacion'));
			if($Tipooperacion->save())
			{
				Input::delete();
				Flash::valid('ok');
			}else{
				Input::delete();
				Flash::warning('error');
			}
		}
	}
	public function editar($id)
	{
		$Tipooperacion = new Tipooperacion();
		if(Input::hasPost('tipooperacion')){
			if($Tipooperacion->update(Input::post('tipooperacion'))){
				Flash::valid('Operación exitosa');
				return Router::redirect();
			}else {
				Flash::error('Falló Operación');
			}
		} else {
			$this->tipooperacion = $Tipooperacion->find_by_id((int)$id);
		}
	}
	public function del($id)
	{
		$Tipooperacion = new Tipooperacion();
		if($Tipooperacion->delete((int)$id)){
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
}