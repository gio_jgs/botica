<?php
Load::models('productos');
class ProductosController extends AppController
{
	protected function before_filter()
	{
		if(Input::isAjax()){ View::response('view');}
	}
	public function index()
	{
		$Productos = new Productos();
		$this->verProductos = $Productos->find();
	}
	public function create()
	{
		if(Input::hasPost('productos'))
		{
			$Productos = new Productos(Input::post('productos'));
			if($Productos->save())
			{
				Input::delete();
				Flash::valid('ok');
				return Router::redirect();
			}else{
				Flash::warning('error');
			}
		}
	}
	public function editar($idproducto)
	{
		$Productos = new Productos();
		if(Input::hasPost('productos')){
			if($Productos->update(Input::post('productos'))){
				Input::delete();
				Flash::valid('OperaciÃ³n exitosa');
				return Router::redirect();
			}else {
				Flash::error('FallÃ³ OperaciÃ³n');
			}
		} else {
			$this->productos = $Productos->find_by_id((int)$idproducto);
		}
	}
	public function del($id)
	{
		$Productos = new Productos();
		if($Productos->delete((int)$id)){
			Input::delete();
			Flash::valid('OK');
			Router::redirect();
		}else{
			Flash::valid('Error');
		}
	}
	public function listarProductos()
	{
		$input = $_GET["q"];
		$Productos = new Productos();
		//$this->clientes = $Clientes->find("conditions: nombres like '%$input%'");
		$this->productos = $Productos->find("nombres like '%$input%'");
	}
}