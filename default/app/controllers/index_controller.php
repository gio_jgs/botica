<?php
/**
 * Controller por defecto si no se usa el routes
 * 
 */
class IndexController extends AppController 
{
	public function index()
	{
		if(Auth::is_valid()){
			Flash::valid('Bienvenido');
		}else{
			return Router::redirect('login/');
			Flash::error('Lo sientimos');
		}	
	}
}
